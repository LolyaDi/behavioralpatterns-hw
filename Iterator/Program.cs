﻿using System;
using System.Collections.Generic;

namespace Iterator
{
    public abstract class Iterator
    {
        // Проверка существования следующего элемента
        public abstract bool HasNext();
        // Переходит к следующему элементу.
        public abstract object Next();
    }

    // Конкретные Итераторы реализуют различные алгоритмы обхода. Эти классы
    // постоянно хранят текущее положение обхода.
    public class ReverseIterator<T> : Iterator
    {
        // Хранит текущее положение обхода. У итератора может быть множество
        // других полей для хранения состояния итерации, особенно когда он
        // должен работать с определённым типом коллекции.
        private int _index;

        private Collection<T> _collection;

        public ReverseIterator(Collection<T> nameCollection)
        {
            _collection = nameCollection;
            _index = _collection.GetCount() - 1;
        }

        public override bool HasNext()
        {
            if (_index >= 0)
                return true;
            return false;
        }

        public override object Next()
        {
            if (HasNext())
                return _collection.GetItems()[_index--];
            return null;
        }
    }

    public interface IContainer
    {
        Iterator GetIterator();
    }

    // Конкретные Коллекции предоставляют один или несколько методов для
    // получения новых экземпляров итератора, совместимых с классом коллекции.
    public class Collection<T> : IContainer
    {
        private List<T> _collection;

        public Collection()
        {
            _collection = new List<T>();
        }

        public List<T> GetItems()
        {
            return _collection;
        }

        public int GetCount()
        {
            return _collection.Count;
        }

        public void AddItem(T item)
        {
            _collection.Add(item);
        }

        public Iterator GetIterator()
        {
            return new ReverseIterator<T>(this);
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var names = new Collection<string>();
            names.AddItem("John");
            names.AddItem("Maria");
            names.AddItem("Emilly");

            for (var iterator = names.GetIterator(); iterator.HasNext();)
            {
                Console.WriteLine($"Name : {(string)iterator.Next()}");
            }

            Console.ReadLine();
        }
    }
}
