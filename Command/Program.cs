﻿using System;

namespace Command
{
    // Интерфейс Команды объявляет метод для выполнения команд.
    public interface ICommand
    {
        void Execute();
        void Cancel();
    }

    // Пример команды, которая делегирует более сложные операции другим
    // объектам, называемым «получателем».
    public class TelevisionCommand : ICommand
    {
        private Television _television;

        public TelevisionCommand(Television television)
        {
            _television = television;
        }

        // Команда может делегировать выполнение любым методом получателя.
        public void Execute()
        {
            _television.TurnOn();
        }

        public void Cancel()
        {
            _television.TurnOff();
        }
    }

    // Классы Получателей содержат некую важную бизнес-логику. Они умеют
    // выполнять все виды операций, связанных с выполнением запроса. Фактически,
    // любой класс может выступать Получателем.
    public class Television
    {
        public void TurnOn()
        {
            Console.WriteLine("TV is on!");
        }

        public void TurnOff()
        {
            Console.WriteLine("TV is off!");
        }
    }

    // Отправитель связан с одной или несколькими командами. Он отправляет
    // запрос команде.
    public class Pult
    {
        private ICommand _command;

        public void SetCommand(ICommand command)
        {
            _command = command;
        }

        // Отправитель не зависит от классов конкретных команд и получателей.
        // Отправитель передаёт запрос получателю косвенно, выполняя команду.
        public void PressOn()
        {
            _command.Execute();
        }

        public void PressOff()
        {
            _command.Cancel();
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var pult = new Pult();
            var television = new Television();

            // Можно сэтить отправителю любые команды
            pult.SetCommand(new TelevisionCommand(television));

            pult.PressOn();

            Console.WriteLine("Too boring...");

            pult.PressOff();

            Console.ReadLine();
        }
    }
}
