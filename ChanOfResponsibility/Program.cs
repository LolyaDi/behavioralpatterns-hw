﻿using System;
using System.Collections.Generic;

namespace ChanOfResponsibility
{
    // Интерфейс Обработчика объявляет метод построения цепочки обработчиков. Он
    // также объявляет метод для выполнения запроса.
    public interface IHandler
    {
        IHandler Next(IHandler handler);
        object Handle(object request);
    }

    // Поведение цепочки по умолчанию может быть реализовано внутри базового
    // класса обработчика.
    public abstract class PaymentHandler : IHandler
    {
        private IHandler _next;

        public virtual object Handle(object request)
        {
            if (_next is null)
                return null;
            return _next.Handle(request);
        }

        public IHandler Next(IHandler handler)
        {
            _next = handler;

            // Возврат обработчика отсюда позволит связать обработчики простым
            // способом, вот так:
            // bankPayment.Next(moneyPayment);
            return _next;
        }
    }

    public class BankPaymentHandler : PaymentHandler
    {
        public override object Handle(object request)
        {
            if (request.ToString() == "Bank payment")
                return $"Doing bank transfering...\n";
            return base.Handle(request);
        }
    }

    public class MoneyPaymentHandler : PaymentHandler
    {
        public override object Handle(object request)
        {
            if (request.ToString() == "Money payment")
                return $"Doing money transfering...\n";
            return base.Handle(request);
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            // Создание цепочки:
            var bankPayment = new BankPaymentHandler();
            var moneyPayment = new MoneyPaymentHandler();

            bankPayment.Next(moneyPayment);

            Display(bankPayment);

            Console.ReadLine();
        }

        // Работа с обработчиком:
        public static void Display(PaymentHandler paymentHandler)
        {
            foreach (var payment in new List<string> { "Money payment", "Bank payment", "Paypal payment" })
            {
                var result = paymentHandler.Handle(payment);

                if (result is null)
                    Console.WriteLine($"{payment} isn't implemented.");
                Console.Write($"{result}");
            }
        }
    }
}
