﻿using System;
using System.Collections.Generic;

namespace Mediator
{
    // Интерфейс Посредника предоставляет методы, используемыми компонентами для
    // уведомления посредника о различных событиях. Посредник может реагировать
    // на эти события  и передавать исполнение другим компонентам.
    public interface IDispatch<T>
    {
        void DistributeMessage(IParticipant<T> sender, T message);
        void Register(IParticipant<T> participant);
    }

    // Конкретные Посредники реализуют совместное поведение, координируя
    // отдельные компоненты.
    public class Dispatch<T> : IDispatch<T>
    {
        private List<IParticipant<T>> _participants;

        public Dispatch()
        {
            _participants = new List<IParticipant<T>>();
        }

        public void Register(IParticipant<T> participant)
        {
            _participants.Add(participant);
        }

        public void DistributeMessage(IParticipant<T> sender, T message)
        {
            foreach (var participant in _participants)
                if (participant != sender)
                    participant.ReceiveMessage(message);
        }
    }

    // Интерфейс Компонента предоставляет методы с соотвествующей функциональностью.
    public interface IParticipant<T>
    {
        void SendMessage(IDispatch<T> dispatch, T message);
        void ReceiveMessage(T message);
    }

    // Конкретные Компоненты реализуют различную функциональность. Они не
    // зависят от других компонентов. Они также не зависят от каких-либо
    // конкретных классов посредников.
    public class Participant<T> : IParticipant<T>
    {
        public string Name { get; set; }

        public void SendMessage(IDispatch<T> dispatch, T message)
        {
            dispatch.DistributeMessage(this, message);
        }

        public void ReceiveMessage(T message)
        {
            Console.WriteLine($"{Name} received <{message.ToString()}>");
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var personA = new Participant<string> { Name = "PersonA" };
            var personB = new Participant<string> { Name = "PersonB" };
            var personC = new Participant<string> { Name = "PersonC" };
           
            var dispatch = new Dispatch<string>();
            
            // включение персон в листинг участвующих в рассылке сообщений
            dispatch.Register(personA);
            dispatch.Register(personB);
            dispatch.Register(personC);
            
            personA.SendMessage(dispatch, "MessageX from PersonA");

            Console.ReadLine();
        }
    }
}